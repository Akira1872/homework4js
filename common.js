let numOne = +prompt("Enter first number");
let numTwo = +prompt("Enter second number");
let mathSign = prompt("Enter a math sign");


while(isNaN(numOne) || Number.isNaN(Number(numOne)) || numOne === null){
    numOne = +prompt("Enter first number", numOne);
}

while(isNaN(numTwo) || Number.isNaN(Number(numTwo)) || numTwo === null){
    numTwo = +prompt("Enter second number", numTwo);
}

while (mathSign !== "+" && mathSign !== "-" && mathSign !== "/" && mathSign !== "*"){
    mathSign = prompt("Enter a math sign", mathSign);
}

let result;

let calcOperation = (numOne, numTwo, mathSign) => {

    switch (mathSign) {
        case "+":
            result = numOne + numTwo;
            break;

        case "-":
            result = numOne - numTwo;
            break;

        case "*":
            result = numOne * numTwo;
            break;
            
        case "/":
            if (numOne === 0 || numTwo === 0){
                console.log('Error: Division by zero');
                return;
            }
            result = numOne / numTwo;
            break;
        default:
            console.log('Mathematical operation is not performed')
        } 
        return result;
    }

calcOperation(numOne, numTwo, mathSign);
    
if (result !== undefined) {
    console.log(result); 
}


